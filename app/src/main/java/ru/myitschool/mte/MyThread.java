package ru.myitschool.mte;


import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MyThread extends Thread{
    FragmentManager fragmentManager;

    int status = 0;
    public boolean isRun = true;

    public MyThread(FragmentManager fragmentManager){
        this.fragmentManager = fragmentManager;
    }

    public void run(){
        while(isRun) {
            if (status == 0) {
                FragmentTransaction ft = fragmentManager.beginTransaction();
                FirstFragment ff = new FirstFragment();
                ft.replace(R.id.output_fragment, ff);
                ft.commit();
                status = 1;
            } else {
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ProceedingFragment ff = new ProceedingFragment();
                ft.replace(R.id.output_fragment, ff);
                ft.commit();
                status = 0;
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

